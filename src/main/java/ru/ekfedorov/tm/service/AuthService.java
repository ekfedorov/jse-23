package ru.ekfedorov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.service.IAuthService;
import ru.ekfedorov.tm.api.service.IUserService;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.exception.system.UserIsLocked;
import ru.ekfedorov.tm.model.User;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.empty.LoginIsEmptyException;
import ru.ekfedorov.tm.exception.empty.PasswordIsEmptyException;
import ru.ekfedorov.tm.util.HashUtil;

import java.util.Optional;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @SneakyThrows
    @Override
    public void checkRoles(@Nullable final Role... roles) {
        if (roles == null || roles.length == 0) return;
        @NotNull final Optional<User> userOptional = getUser();
        if (!userOptional.isPresent()) throw new AccessDeniedException();
        @NotNull final User user = userOptional.get();
        @Nullable final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (@Nullable final Role item : roles) {
            if (item != null && item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @NotNull
    @SneakyThrows
    public Optional<User> getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        return userService.findOneById(userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @SneakyThrows
    @Override
    public void login(
            @Nullable final String login, @Nullable final String password
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        @NotNull final Optional<User> userOptional = userService.findByLogin(login);
        if (!userOptional.isPresent()) throw new AccessDeniedException();
        @Nullable final User user = userOptional.get();
        if (user.isLock()) throw new UserIsLocked();
        @Nullable final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @SneakyThrows
    @Override
    public void registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        userService.create(login, password, email);
    }

}
