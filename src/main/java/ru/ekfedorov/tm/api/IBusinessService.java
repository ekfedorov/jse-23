package ru.ekfedorov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.AbstractBusinessEntity;

import java.util.Optional;

public interface IBusinessService<E extends AbstractBusinessEntity>
        extends IBusinessRepository<E>, IService<E> {

    @NotNull
    Optional<E> changeStatusById(
            @Nullable String userId, @Nullable String id, @Nullable Status status
    );

    @NotNull
    Optional<E> changeStatusByIndex(
            @Nullable String userId, @Nullable Integer index, @Nullable Status status
    );

    @NotNull
    Optional<E> changeStatusByName(
            @Nullable String userId, @Nullable String name, @Nullable Status status
    );

    @NotNull
    Optional<E> finishById(@Nullable String userId, @Nullable String id);

    @NotNull
    Optional<E> finishByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Optional<E> finishByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Optional<E> startById(@Nullable String userId, @Nullable String id);

    @NotNull
    Optional<E> startByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Optional<E> startByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Optional<E> updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Optional<E> updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
