package ru.ekfedorov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    Optional<User> findByLogin(@NotNull String login);

    @NotNull
    Boolean isLoginExist(@NotNull String login);

    void removeByLogin(@NotNull String login);

}
