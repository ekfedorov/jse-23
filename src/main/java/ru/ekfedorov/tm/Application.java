package ru.ekfedorov.tm;

import ru.ekfedorov.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(final String[] args) throws Exception {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
