package ru.ekfedorov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> list = new ArrayList<>();

    @NotNull
    @Override
    public E add(final @NotNull E entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return list;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        return list.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Optional<E> findOneById(@NotNull final String id) {
        return list.stream()
                .filter(entity -> id.equals(entity.getId()))
                .findFirst();
    }

    @NotNull
    @Override
    public E remove(@NotNull final E entity) {
        list.remove(entity);
        return entity;
    }

    @NotNull
    @Override
    public Optional<E> removeOneById(@NotNull final String id) {
        return findOneById(id).map(this::remove);
    }

}
