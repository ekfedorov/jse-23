package ru.ekfedorov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Evgeniy Fedorov");
        System.out.println("ekfedorov@tsconsulting.com");
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

}
