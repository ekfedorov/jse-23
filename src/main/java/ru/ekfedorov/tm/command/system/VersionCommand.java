package ru.ekfedorov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.18.0");
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

}
