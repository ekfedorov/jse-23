package ru.ekfedorov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.service.IProjectService;
import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.exception.system.NullProjectException;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Optional;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by index.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (serviceLocator == null) throw new NullObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final Optional<Project> project = projectService.findOneByIndex(userId, index);
        if (!project.isPresent()) throw new NullProjectException();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Optional<Project> projectUpdated =
                projectService.updateByIndex(userId, index, name, description);
        if (!projectUpdated.isPresent()) throw new NullProjectException();
    }

    @NotNull
    @Override
    public String name() {
        return "project-update-by-index";
    }

}
