package ru.ekfedorov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.service.IProjectService;
import ru.ekfedorov.tm.api.service.IProjectTaskService;
import ru.ekfedorov.tm.api.service.ITaskService;
import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.exception.system.NullTaskException;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Optional;

public final class TaskUnbindByProjectIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unbind task from project.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (serviceLocator == null) throw new NullObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        @NotNull final Optional<Task> task = projectTaskService.unbindTaskFromProject(userId, id);
        if (!task.isPresent()) throw new NullTaskException();
    }

    @NotNull
    @Override
    public String name() {
        return "unbind-task-from-project";
    }

}
